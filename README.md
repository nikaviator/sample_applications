# Sample applications for ROS combined with a (simulated) Franka Emika Panda robots

⚠ This repository must be checked out in a [catkin](http://wiki.ros.org/catkin)
workspace containing the [franka_description](https://git-st.inf.tu-dresden.de/ceti/ros/franka_description),
[panda_moveit_config](https://git-st.inf.tu-dresden.de/ceti/ros/panda_moveit_config),
and [panda_simulation](https://git-st.inf.tu-dresden.de/ceti/ros/panda_simulation) ROS packages provided here.
For more information and installation instructions, please consult the [main project](https://git-st.inf.tu-dresden.de/ceti/ros/panda_gazebo_workspace).

Contains different examples for the planning and execution of robotic motions with MoveIt in ROS.

## Example Applications

Please note that all examples must be run in a built catkin workspace from within
a shell with all ROS paths properly set up (i.e., after running `source devel/setup.sh`).

### Minimal "Hello World" Example

This example of an execution of a simple motion.

- Command: `roslaunch sample_applications sample_minimal_simulation.launch`
- Expected results:
    - A Gazebo window opens in which the executed robot motion can be observed.
    - The motion starts immediately after the the system has loaded.

### Execution of a Simple Motion with Tooling

This is the same example as the first one, but this time, additional tooling is loaded.

- Command: `roslaunch sample_applications sample_simple_simulation.launch`
- Expected results:
    - A Gazebo window to observe the executed robot motion opens
    - An rqt window to show log messages opens
    - An RViz window to see the plan and trigger the motion open.
    - The motion does **not** start automatically, instead it must be triggered by pressing *next* in the RvizVisualToolsGUI within RViz


### Execution of a Simple Motion Costrained by a Blocking Object

In this example, an object can be observed in the RViz planning window. This object constrains the motion of the robot, such that the robot moves around it.

- Command: `roslaunch sample_applications sample_constraint_simulation.launch`
- Expected results:
    - A Gazebo window to observe the executed robot motion opens
    - An rqt window to show log messages opens
    - An RViz window to see the plan and trigger the motion open.
    - The motion does **not** start automatically, instead it must be triggered by pressing *next* in the RvizVisualToolsGUI within RViz

### Execution of a Velocity Constraint Cartesian Trajectory

- Command: `roslaunch sample_applications timed_cartesian_simulation.launch`
- Expected results:
    - A Gazebo window to observe the executed robot motion opens
    - An rqt window to show log messages opens
    - An RViz window to see the plan and trigger the motion open.
    - The motion does **not** start automatically, instead it must be triggered by pressing *next* in the RvizVisualToolsGUI within RViz

### Planning and Simulation based on RViz

This is an example to use RViz and Gazebo interactively. Motions can be planned and executed from the RViz GUI.

- Command: `roslaunch sample_applications simulation.launch`
- Expected results:
    - RViz window to control the robot
    - Gazebo window to observe the executed robot motion
    - rqt to show log messages
    - The robot does not move automatically, but can be controlled with RViz (cf. the [tutorial](http://docs.ros.org/melodic/api/moveit_tutorials/html/doc/quickstart_in_rviz/quickstart_in_rviz_tutorial.html))




